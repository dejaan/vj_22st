package ba.unsa.etf.rma.vj_22st;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Field;

public class MovieDetailActivity extends AppCompatActivity {
    private TextView title;
    private TextView genre;
    private TextView overview;
    private TextView releaseDate;
    private TextView homepage;
    private ListView actors;
    private ArrayAdapter<String> adapter;
    private ImageView imageView;
    private InterMovieDetailPresenter presenter;

    public InterMovieDetailPresenter getPresenter() {
        if (presenter == null) {
            presenter = new MovieDetailPresenter(this);
        }
        return presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        getPresenter().create(getIntent().getStringExtra("title"), getIntent().getStringExtra("overview"), getIntent().getStringExtra("releaseDate"),
                getIntent().getStringExtra("genre"),  getIntent().getStringExtra("homepage"),getIntent().getStringArrayListExtra("actors"));

        title = findViewById(R.id.title);
        genre = findViewById(R.id.genre);
        overview = findViewById(R.id.overview);
        releaseDate = findViewById(R.id.releaseDate);
        homepage = findViewById(R.id.homepage);
        actors = findViewById(R.id.actors);
        imageView = findViewById(R.id.icon);

        Movie movie = getPresenter().getMovie();
        title.setText(movie.getTitle());
        genre.setText(movie.getGenre());
        overview.setText(movie.getOverview());
        releaseDate.setText(movie.getReleaseDate());
        homepage.setText(movie.getHomepage());

        String genreMatch = movie.getGenre();
        try {
            Class res = R.drawable.class;
            Field field = res.getField(genreMatch);
            int drawableId = field.getInt(null);
            imageView.setImageResource(drawableId);
        } catch (Exception e) {
            imageView.setImageResource(R.drawable.picture1);
        }
        adapter = new ArrayAdapter <String>(this,android.R.layout.simple_list_item_1, movie.getActors());
        actors.setAdapter(adapter);
    }
}
