package ba.unsa.etf.rma.vj_22st;

public interface InterMovieListPresenter {
    void refreshMovies();
}
