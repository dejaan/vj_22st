package ba.unsa.etf.rma.vj_22st;

import java.util.ArrayList;

public class MovieListInteractor implements InterMovieListInteractor {

    @Override
    public ArrayList<Movie> get() {
        return MoviesModel.movies;
    }
}
