package ba.unsa.etf.rma.vj_22st;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements InterMovieListView {
    private EditText searchTxtFld;
    private Button searchBtn;
    private ListView entriesListView;
    private InterMovieListPresenter presenter;
    private MovieListAdapter adapter;

    public InterMovieListPresenter getPresenter() {
        if (presenter == null) {
            presenter = new MovieListPresenter(this, this);
        }
        return presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new MovieListAdapter(getApplicationContext(), R.layout.list_element, new ArrayList<Movie>());
        entriesListView = (ListView)findViewById(R.id.moviesListView);
        entriesListView.setAdapter(adapter);
        entriesListView.setOnItemClickListener(listItemClickListener);
        getPresenter().refreshMovies();
    }

    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent movieDetailIntent = new Intent(MainActivity.this, MovieDetailActivity.class);
            Movie movie = adapter.getItem(position);
            movieDetailIntent.putExtra("title", movie.getTitle());
            movieDetailIntent.putExtra("genre", movie.getGenre());
            movieDetailIntent.putExtra("homepage", movie.getHomepage());
            movieDetailIntent.putExtra("overview", movie.getOverview());
            movieDetailIntent.putExtra("releaseDate", movie.getReleaseDate());
            movieDetailIntent.putStringArrayListExtra("actors", movie.getActors());
            MainActivity.this.startActivity(movieDetailIntent);
        }
    };

    @Override
    public void setMovies(ArrayList<Movie> movies) {
        adapter.setMovies(movies);
    }

    @Override
    public void notifyMovieListDataSetChanged() {
        adapter.notifyDataSetChanged();
    }
}
