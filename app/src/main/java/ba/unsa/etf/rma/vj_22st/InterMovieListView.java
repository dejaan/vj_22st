package ba.unsa.etf.rma.vj_22st;

import java.util.ArrayList;

public interface InterMovieListView {
    void setMovies(ArrayList<Movie> movies);
    void notifyMovieListDataSetChanged();
}
