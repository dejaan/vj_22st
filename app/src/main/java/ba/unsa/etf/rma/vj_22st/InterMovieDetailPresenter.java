package ba.unsa.etf.rma.vj_22st;

import java.util.ArrayList;

public interface InterMovieDetailPresenter {
    void create(String title, String overview, String releaseDate, String genre, String homepage, ArrayList<String> actors);
    Movie getMovie();
}
