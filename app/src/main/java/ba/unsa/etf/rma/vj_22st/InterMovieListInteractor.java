package ba.unsa.etf.rma.vj_22st;

import java.util.ArrayList;

public interface InterMovieListInteractor {
    ArrayList<Movie> get();
}
