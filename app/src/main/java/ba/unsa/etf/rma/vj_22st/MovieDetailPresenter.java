package ba.unsa.etf.rma.vj_22st;

import android.content.Context;

import java.util.ArrayList;

public class MovieDetailPresenter implements InterMovieDetailPresenter {
    private Context context;
    private Movie movie;

    public MovieDetailPresenter(Context context) {
        this.context    = context;
    }

    @Override
    public void create(String title, String overview, String releaseDate, String genre, String homepage, ArrayList<String> actors) {
        this.movie = new Movie(title,overview,releaseDate,homepage,genre,actors);
    }

    @Override
    public Movie getMovie() {
        return movie;
    }
}
