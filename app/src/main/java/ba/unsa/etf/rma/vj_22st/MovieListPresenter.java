package ba.unsa.etf.rma.vj_22st;

import android.content.Context;

public class MovieListPresenter implements InterMovieListPresenter {
    private InterMovieListInteractor interactor;
    private InterMovieListView view;
    private Context context;

    public MovieListPresenter(InterMovieListView view, Context context) {
        this.interactor = new MovieListInteractor();
        this.view = view;
        this.context = context;
    }

    @Override
    public void refreshMovies() {
        view.setMovies(interactor.get());
        view.notifyMovieListDataSetChanged();
    }
}
